Source: fpc
Section: devel
Priority: optional
Maintainer: Pascal Packaging Team <pkg-pascal-devel@lists.alioth.debian.org>
Uploaders:
 Abou Al Montacir <abou.almontacir@sfr.fr>,
 Paul Gevers <elbrus@debian.org>,
 Peter Michael Green <plugwash@debian.org>,
Standards-Version: 4.7.0
Build-Depends:
 debhelper (>= 13),
 dh-exec (>=0.22),
 fp-compiler,
 fp-units-fcl,
 fp-utils,
 ghostscript,
 help2man,
 libncurses-dev,
 mawk | awk,
 po-debconf,
 txt2man,
Build-Depends-Indep:
 hevea,
 rdfind,
 symlinks
Vcs-Git: https://salsa.debian.org/pascal-team/fpc.git
Vcs-Browser: https://salsa.debian.org/pascal-team/fpc
Homepage: https://www.freepascal.org/

Package: fpc${PACKAGESUFFIX}
Architecture: all
Depends:
 fp-compiler${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-ide${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-base${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-db${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-fcl${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-fv${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-gfx${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-gtk2${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-math${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-misc${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-multimedia${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-net${PACKAGESUFFIX} (>= ${binary:Version}),
 fp-units-rtl${PACKAGESUFFIX} (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-utils${PACKAGESUFFIX}
Suggests:
 fp-docs${PACKAGESUFFIX} (>= ${source:Upstream-Version}),
 lazarus
Provides:
 fpc
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - SDK${PACKAGESUFFIX} suite
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This metapackage pulls in all the FPC packages provided for this
 architecture. Experienced users may instead prefer to install the particular
 packages they require by hand.

Package: fpc-source${PACKAGESUFFIX}
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fpc-source
Breaks:
 fpc-src
Replaces:
 fpc-src
Description: Free Pascal - SDK source code
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal's own source code. It is meant to be used by
 the Lazarus IDE.

Package: fp-compiler${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 binutils,
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-utils${PACKAGESUFFIX} (>= ${binary:Version})
Suggests:
 fp-docs${PACKAGESUFFIX} (>= ${source:Upstream-Version}),
Provides:
 fp-compiler
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - compiler
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This package contains the command line compiler.

Package: fp-ide${PACKAGESUFFIX}
Architecture: any
Multi-Arch: foreign
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-utils${PACKAGESUFFIX} (>= ${binary:Version})
Suggests:
 fp-docs${PACKAGESUFFIX} (>= ${source:Upstream-Version})
Provides:
 fp-ide
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - IDE
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This package contains the Integrated Development Environment (IDE). The IDE
 has an internal compiler.

Package: fp-utils${PACKAGESUFFIX}
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-compiler${PACKAGESUFFIX} (= ${binary:Version})
Breaks:
 fp-compiler${PACKAGESUFFIX} (<= 3.0.4+dfsg-11),
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fp-compiler${PACKAGESUFFIX} (<= 3.0.4+dfsg-11),
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - utilities
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains some handy utilities for use with the Free Pascal
 Compiler:
  * data2inc   convert binary/text data to include files;
  * fpcmake    create Makefile from Makefile.fpc;
  * h2pas      convert .h files to Pascal units;
  * plex/pyacc Pascal Lex and Yacc implementations;
  * ppdep      create a dependency file for use with Makefiles;
  * ppudump    dump the information stored in a .ppu (unit) file;
  * ppufiles   show needed files for units;
  * ppumove    place multiple units in a shared library;
  * ptop       beautify source.

Package: fp-docs${PACKAGESUFFIX}
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 fp-docs
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - documentation
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package provides documentation for the Free Pascal Compiler in HTML
 format.

Package: fp-units-rtl${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-compiler${PACKAGESUFFIX} (= ${binary:Version})
Provides:
 fp-units-rtl,
 fpc-abi${PACKAGESUFFIX}
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
 fp-compiler${PACKAGESUFFIX} (<= 3.2.2+dfsg-23)
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
 fp-compiler${PACKAGESUFFIX} (<= 3.2.2+dfsg-23)
Description: Free Pascal - runtime libraries
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the RunTime Libraries for the Free Pascal Compiler.

Package: fp-units-base${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 uuid-dev
Provides:
 fp-units-base
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - base units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units for common libraries (some of which
 are also required by the Free Component Library): NCurses, X11 (Xlib,
 Xutil), and ZLib.

Package: fp-units-fcl${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-base${PACKAGESUFFIX} (= ${binary:Version}),
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-fcl
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - Free Component Library
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the Free Component Library for the Free Pascal Compiler.

Package: fp-units-fv${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-fv
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - Free Vision units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the Free Vision units for the Free Pascal Compiler
 (which provide a framework for developing text user interfaces).

Package: fp-units-gtk2${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-fcl${PACKAGESUFFIX} (= ${binary:Version}),
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 libgtk2.0-dev,
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-gtk2
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - GTK+ 2.x units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units and examples to create
 programs with GTK+ 2.x.

Package: fp-units-db${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 firebird-dev,
 freetds-dev,
 libgdbm-dev,
 default-libmysqlclient-dev,
 libpq-dev,
 libsqlite3-dev,
 pxlib-dev,
 unixodbc-dev
Provides:
 fp-units-db
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - database-library units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units with bindings for GDBM, Interbase,
 MySQL, PostgreSQL, ODBC, Oracle, and SQLite.

Package: fp-units-gfx${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-base${PACKAGESUFFIX} (= ${binary:Version}),
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-gfx
Recommends:
 libcairo2-dev,
 libforms-dev,
 libgd-dev,
 libgl-dev,
 libgraphviz-dev,
 libpng-dev,
 libxxf86dga-dev,
 libxxf86vm-dev
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - graphics-library units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units with bindings for cairo, forms, ggi,
 graph, libgd, libpng, opengl, and svgalib.
 .
 SVGALib is no longer packaged by Debian and should be installed manually by
 users who want to link against it.

Package: fp-units-net${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-net
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - networking units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units for creating network tools: D-Bus,
 httpd-1.3, httpd-2.0, httpd-2.2, ldap, libasync, libcurl, netdb, openssl,
 and pcap.

Package: fp-units-math${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 libgmp-dev
Provides:
 fp-units-math
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - math units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal math interfacing units for:
  * gmp (the GNU Multiple Precision arithmetic library);
  * numlib (numerical computing);
  * proj4 (cartographic projections);
  * symbolic (symbolic computing).

#note: when backporting to stretch or earlier disable ncurses6.patch
#and then change the libncursesw5-dev breaks to >= 6.1+20180210
Package: fp-units-misc${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-misc
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
 libncursesw5-dev (<< 6.1+20180210)
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - miscellaneous units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains miscellaneous Free Pascal units: fppkg (the FPC
 packaging system), PasZLib (a Pascal-only zlib implementation), and Utmp.

Package: fp-units-multimedia${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 a52dec-dev,
 libdts-dev,
 libmad0-dev,
 libmodplug-dev,
 libogg-dev,
 libsdl-mixer1.2-dev,
 libvorbis-dev,
 libvlc-dev
Provides:
 fp-units-multimedia
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - multimedia units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal multimedia units: a52, dts, mad, modplug,
 oggvorbis, openal, and vlc.

Package: fp-units-i386${PACKAGESUFFIX}
Architecture: i386
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-i386
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - Kylix compatibility units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the deprecated Free Pascal libc unit for the i386
 architecture (used for compatibility with Borland's Kylix).

Package: fp-units-wasm${PACKAGESUFFIX}
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-wasm
Breaks:
 fpc (<= ${UPSTREAM_VERSION}-0),
Replaces:
 fpc (<= ${UPSTREAM_VERSION}-0),
Description: Free Pascal - Web assembly support units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal web assembly support units: pas2js,
 utils-pas2js, webidl ...

Package: fpc
Architecture: all
Depends:
 fpc${PACKAGESUFFIX} (= ${binary:Version}),
 fp-docs${PACKAGESUFFIX},
 fp-utils,
 ${misc:Depends}
Description: Free Pascal - SDK suite dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This dependency package always depends on the latest available version of
 the metapackage pulling in all the FPC packages provided for this
 architecture. Experienced users may instead prefer to install the particular
 packages they require by hand.

Package: fpc-source
Architecture: all
Multi-Arch: foreign
Depends:
 fpc-source${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - SDK source code dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal's own source code. It is meant to be used by
 the Lazarus IDE.

Package: fp-compiler
Architecture: any
Multi-Arch: same
Depends:
 fp-compiler${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - compiler dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This dependency package always depends on the latest available version of
 the package containing the command line compiler.

Package: fp-ide
Architecture: all
Multi-Arch: foreign
Depends:
 fp-ide${PACKAGESUFFIX} (>= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - IDE dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 Extensions have been added to the language, such as function overloading,
 creation and linking of shared libraries, and Delphi language extensions
 including classes, exceptions, ANSI strings, and open arrays.
 .
 This dependency package always depends on the latest available version of
 the package containing the Integrated Development Environment (IDE). The IDE
 has an internal compiler.

Package: fp-utils
Architecture: all
Multi-Arch: foreign
Depends:
 fp-utils${PACKAGESUFFIX} (>= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - utilities dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing some handy utilities for use with the Free Pascal
 Compiler:
  * data2inc   convert binary/text data to include files;
  * fpcmake    create Makefile from Makefile.fpc;
  * h2pas      convert .h files to Pascal units;
  * plex/pyacc Pascal Lex and Yacc implementations;
  * ppdep      create a dependency file for use with Makefiles;
  * ppudump    dump the information stored in a .ppu (unit) file;
  * ppufiles   show needed files for units;
  * ppumove    place multiple units in a shared library;
  * ptop       beautify source.

Package: fp-docs
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 fp-docs${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - documentation dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing documentation for the Free Pascal Compiler in HTML
 format.

Package: fp-units-rtl
Architecture: any
Multi-Arch: same
Depends:
 fp-units-rtl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - runtime libraries dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the RunTime Libraries for the Free Pascal Compiler.

Package: fp-units-base
Architecture: any
Multi-Arch: same
Depends:
 fp-units-base${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - base units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units for common libraries (some of which
 are also required by the Free Component Library): NCurses, X11 (Xlib,
 Xutil), and ZLib.

Package: fp-units-fcl
Architecture: any
Multi-Arch: same
Depends:
 fp-units-fcl${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - Free Component Library dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Component Library for the Free Pascal Compiler.

Package: fp-units-fv
Architecture: any
Multi-Arch: same
Depends:
 fp-units-fv${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - Free Vision units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Vision units for the Free Pascal Compiler
 (which provide a framework for developing text user interfaces).

Package: fp-units-gtk2
Architecture: any
Multi-Arch: same
Depends:
 fp-units-gtk2${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - GTK+ 2.x units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units and examples to create
 programs with GTK+ 2.x.

Package: fp-units-db
Architecture: any
Multi-Arch: same
Depends:
 fp-units-db${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - database-library units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units with bindings for GDBM, Interbase,
 MySQL, PostgreSQL, ODBC, Oracle, and SQLite.

Package: fp-units-gfx
Architecture: any
Multi-Arch: same
Depends:
 fp-units-gfx${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - graphics-library units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units with bindings for cairo, forms, ggi,
 graph, libgd, libpng, opengl, and svgalib.
 .
 SVGALib is no longer packaged by Debian and should be installed manually by
 users who want to link against it.

Package: fp-units-net
Architecture: any
Multi-Arch: same
Depends:
 fp-units-net${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - networking units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units for creating network tools: D-Bus,
 httpd-1.3, httpd-2.0, httpd-2.2, ldap, libasync, libcurl, netdb, openssl,
 and pcap.

Package: fp-units-math
Architecture: any
Multi-Arch: same
Depends:
 fp-units-math${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - math units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal math interfacing units for:
  * gmp (the GNU Multiple Precision arithmetic library);
  * numlib (numerical computing);
  * proj4 (cartographic projections);
  * symbolic (symbolic computing).

Package: fp-units-misc
Architecture: any
Multi-Arch: same
Depends:
 fp-units-misc${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - miscellaneous units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing miscellaneous Free Pascal units: fppkg (the FPC
 packaging system), PasZLib (a Pascal-only zlib implementation), and Utmp.

Package: fp-units-multimedia
Architecture: any
Multi-Arch: same
Depends:
 fp-units-multimedia${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - multimedia units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal multimedia units: a52, dts, mad, modplug,
 oggvorbis, openal, and vlc.

Package: fp-units-i386
Architecture: i386
Depends:
 fp-units-i386${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - Kylix compatibility units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the deprecated Free Pascal libc unit for the i386
 architecture (used for compatibility with Borland's Kylix).

Package: fp-units-wasm
Architecture: any
Multi-Arch: same
Depends:
 fp-units-wasm${PACKAGESUFFIX} (= ${binary:Version}),
 ${misc:Depends}
Description: Free Pascal - Web assembly support units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Pascal web assembly support units: pas2js,
 utils-pas2js, webidl ...
